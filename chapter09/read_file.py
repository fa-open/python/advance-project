# 500G的文件，特殊，这个文件只有一行，使用{|}来做分隔符
# 一行行读取出来，写道数据库中
def myreadlines(f, newline):
    buf = ""
    while True:
        while newline in buf:
            pos = buf.index(newline)
            yield buf[:pos]
            buf = buf[pos + len(newline):]
        chunk = f.read(4096*10)

        # 处理边界情况
        if not chunk:
            # 说明已经读到了文件结尾
            yield buf
            break

        buf += chunk

with open("input.txt") as f:
    for line in myreadlines(f, "{|}"):
        print(line)
