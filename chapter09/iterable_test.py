# 什么是迭代协议
# 迭代器是访问集合内元素的一种方式，一般用来遍历数据
# 迭代器和以下标的访问方式不一样，迭代器是不能返回的，爹代替提供了一种惰性方式数据的方式
# [] 下标访问的背后逻辑是 __getitem__

from collections.abc import Iterable, Iterator
a = [1, 2]
iter_rator = iter(a)
# True，表示a是一个可迭代
print(isinstance(a, Iterable))
# False，表示a并不是一个迭代器
print(isinstance(a, Iterator))
print(isinstance(iter_rator, Iterator))
