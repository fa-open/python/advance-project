class Company(object):
    def __init__(self, employee_list):
        self.employee = employee_list

    def __iter__(self):
        return MyIterator(self.employee)

    # 以__开头的为魔法函数，通常使用python自定义的一些魔法函数
    # def __getitem__(self, item):
    #     return self.employee[item]

from collections.abc import Iterator
class MyIterator(Iterator):
    def __init__(self, list):
        self.iter_list = list
        self.index = 0
    def __next__(self):
        # 真正返回迭代值的逻辑
        try:
            word = self.iter_list[self.index]
        except IndexError:
            raise StopIteration
        self.index += 1
        return word

if __name__ == '__main__':
    company = Company(["tom", "bob", "jane"])
    my_itor = iter(company)
    while True:
        try:
            print(next(my_itor))
        except StopIteration:
            pass
    # for item in company:
    #     print(item)

