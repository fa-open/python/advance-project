from multiprocessing import Process, Queue, Manager, Pipe
# from queue import Queue
import time


# def producer(queue):
#     queue.put("a")
#     time.sleep(2)
#
# def consumer(queue):
#     time.sleep(2)
#     data = queue.get()
#     print(data)
#
# if __name__ == '__main__':
#     queue = Manager().Queue(10)
#     my_producer = Process(target=producer, args=(queue,))
#     my_consumer = Process(target=consumer, args=(queue,))
#     my_producer.start()
#     my_consumer.start()
#     my_producer.join()
#     my_consumer.join()

# 共享全局变量不能使用于多进程编程，可以适用于多线程

# multiprocess的Queue不能用于pool进程池
# pool中的进程间通信需要使用manager中的queue

# 通过pipe实现进程间通信，pipe只能适用于两个进程
# pipe的性能高于queue

# def producer(pipe):
#     time.sleep(2)
#     pipe.send("hello")
#
# def consumer(pipe):
#     data = pipe.recv()
#     print(data)
#
# if __name__ == '__main__':
#     receive_pipe, send_pipe = Pipe()
#     # pipe只能适用于两个进程
#     my_producer = Process(target=producer, args=(send_pipe,))
#     my_consumer = Process(target=consumer, args=(receive_pipe,))
#
#     my_producer.start()
#     my_consumer.start()
#     my_producer.join()
#     my_consumer.join()

# Manager().dict()可用于进程间共享一个内存空间
def add_data(p_dict, key, value):
    p_dict[key] = value

if __name__ == '__main__':
    progress_dict = Manager().dict()

    first_progress = Process(target=add_data, args=(progress_dict, "bobby1", 1))
    second_progress = Process(target=add_data, args=(progress_dict, "bobby2", 3))

    first_progress.start()
    second_progress.start()
    first_progress.join()
    second_progress.join()

    print(progress_dict)

