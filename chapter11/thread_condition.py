# 条件变量中，用于复杂的线程间通信
import threading


# class XiaoAi(threading.Thread):
#     def __init__(self, lock):
#         super().__init__(name="小爱")
#         self.lock = lock
#
#     def run(self):
#         self.lock.acquire()
#         print("{}: 在".format(self.name))
#         self.lock.release()
#
#
# class TianMao(threading.Thread):
#     def __init__(self, lock):
#         super().__init__(name="天猫精灵")
#         self.lock = lock
#
#     def run(self):
#         self.lock.acquire()
#         print("{}: 小爱同学".format(self.name))
#         self.lock.release()


# if __name__ == '__main__':
#     lock = threading.Lock()
#     xiaoai = XiaoAi(lock)
#     tianmao = TianMao(lock)
#
#     tianmao.start()
#     xiaoai.start()


# 通过condition完成协同
class XiaoAi(threading.Thread):
    def __init__(self, cond):
        super().__init__(name="小爱")
        self.cond = cond

    def run(self):
        with self.cond:
            self.cond.wait()
            print("{}: 在".format(self.name))
            self.cond.notify()
            self.cond.wait()

            print("{}: 你也好啊".format(self.name))
            self.cond.notify()
            self.cond.wait()


class TianMao(threading.Thread):
    def __init__(self, cond):
        super().__init__(name="天猫精灵")
        self.cond = cond

    def run(self):
        with self.cond:
            print("{}: 小爱同学".format(self.name))
            self.cond.notify()
            self.cond.wait()

            print("{}: 你好啊".format(self.name))
            self.cond.notify()
            self.cond.wait()


if __name__ == '__main__':
    cond = threading.Condition()
    xiaoai = XiaoAi(cond)
    tianmao = TianMao(cond)

    # 启动顺序很重要
    # 在调用with cond之后才能调用wait或这notify方法
    # condition有两层锁，一把底层锁会在线程调用了wait方法的时候释放
    # 上面的锁会在每次调用wait的时候分配一把并放入到cond的等待队列中，等待notify方法的唤醒
    xiaoai.start()
    tianmao.start()
