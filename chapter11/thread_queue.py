# 线程间通信

import time
import threading
from concurrent.futures import Future


detail_url_list = []

def get_detail_html(url):
    # 爬取文章详情页
    print("get detail html started")
    time.sleep(2)
    print("get detail html end")


def get_detail_url(url):
    # 爬取文章列表页
    global detail_url_list
    print("get detail url started")
    time.sleep(4)
    for i in range(20):
        detail_url_list.append("http://aa/{id}".format(id=i))
    print("get detail url end")

# 线程间通行方式-共享变量


# if __name__ == '__main__':
#     thread1 = threading.Thread(target=get_detail_html, args=("", ))
#     thread2 = threading.Thread(target=get_detail_url, args=("", ))
#     # thread1.setDaemon(True)
#     # thread2.setDaemon(True)
#
#     start_time = time.time()
#     thread1.start()
#     thread2.start()
#
#     thread1.join()
#     thread2.join()
#
#     # 当主线程退出的时候，子线程kill掉
#     print("last time: {}".format(time.time() - start_time))


# 2. 通过继承Thread来实现多线程
class GetDetailHtml(threading.Thread):
    def __init__(self, name):
        super().__init__(name=name)

    def run(self):
        print("get detail html started")
        time.sleep(2)
        print("get detail html end")


class GetDetailUrl(threading.Thread):
    def __init__(self, name):
        super().__init__(name=name)

    def run(self):
        print("get detail url started")
        time.sleep(4)
        print("get detail url end")


if __name__ == '__main__':
    thread1 = GetDetailHtml("get_detail_html")
    thread2 = GetDetailUrl("get_detail_url")
    start_time = time.time()
    thread1.start()
    thread2.start()

    thread1.join()
    thread2.join()

    # 当主线程退出的时候，子线程kill掉
    print("last time: {}".format(time.time() - start_time))
