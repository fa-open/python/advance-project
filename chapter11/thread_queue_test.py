# 通过queue的方式进行线程间通信
from queue import Queue


query_list = Queue()
# 线程安全
query_list.put("aaa")
query_list.get()

# 调用了task_done之后，queue才会join退出
query_list.task_done()
query_list.join()


