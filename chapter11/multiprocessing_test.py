# import os
# import time
# # fork只能用于linxu、unix中
# pid = os.fork()
# print("bobby")
# if pid == 0:
#     print("子进程： {}，父进程：{}".format(os.getpid(), os.getppid()))
# else:
#     print("我是父进程：{}".format(pid))
#
# time.sleep(2)


from concurrent.futures import ProcessPoolExecutor
import multiprocessing
import time

# 多进程编程
def get_html(n):
    time.sleep(n)
    print("sub progress end")
    return n

# class MyProgress(multiprocessing.Process):
#     def run(self, n):
#         time.sleep(n)
#         print("sub progress end")
#         return n

if __name__ == '__main__':
    # progress = multiprocessing.Process(target=get_html, args=(2, ))
    # print(progress.pid)
    # progress.start()
    # print(progress.pid)
    # progress.join()
    # print("main progress end")

    # 使用进程池
    pool = multiprocessing.Pool(multiprocessing.cpu_count())
    # result = pool.apply_async(get_html, args=(3,))
    # # 等待所有任务完成
    # pool.close()
    # pool.join()
    # print(result.get())

    #imap
    # for result in pool.imap(get_html, [1, 5, 3]):
    #     print(result)

    # 无序
    for result in pool.imap_unordered(get_html, [1, 5, 3]):
        print(result)



