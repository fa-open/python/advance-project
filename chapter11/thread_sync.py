#
from threading import Lock, RLock # 可重入的锁

# 在同一个线程中，可以连续调用多次acquire，一定要注意acquire的次数要和release的次数一致
total = 0
lock = RLock()

def add(lock):
    global total
    for i in range(1000000):
        lock.acquire()
        lock.acquire()
        total += 1
        lock.release()
        lock.release()

def desc():
    global total
    global lock
    for i in range(1000000):
        lock.acquire()
        total -= 1
        lock.release()

import threading
thread1 = threading.Thread(target=add, args=(lock,))
thread2 = threading.Thread(target=desc)
thread1.start()
thread2.start()

thread1.join()
thread2.join()
print(total)

# 1. 用锁会影响性能
# 2. 锁会引起死锁
# 死锁的情况A(a,b)
"""
A(a,b)
acquire(a)
acquire(b)

B(a,b)
acquire(b)
acquire(a)
"""


