# 既然我们重写B的构造函数，为什么还要去调用super？
# super到底执行顺序是什么样的？
class A:
    def __init__(self):
        print("A")

class B(A):
    def __init__(self):
        print("B")
        super().__init__()

class C(A):
    def __init__(self):
        print("C")
        super().__init__()

class D(B, C):
    def __init__(self):
        print("D")
        super(D, self).__init__()

if __name__ == "__main__":
    # (<class '__main__.D'>, <class '__main__.B'>, <class '__main__.C'>, <class '__main__.A'>, <class 'object'>)
    # 可见，super实际上是调用的__mro__链条的上一个类，即按照__mro__链条的顺序依次调用
    print(D.__mro__)
    d = D()