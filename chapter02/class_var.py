class A:
    aa = 1
    def __init__(self, x, y):
        self.x = x
        self.y = y

a = A(2, 3)
# 这里修改的是类A的类变量aa
A.aa = 11
# 这里修改的是实例a的实例变量aa
a.aa = 100
print(a.x, a.y, a.aa)
print(A.aa)
