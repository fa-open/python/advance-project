class Company(object):
    def __init__(self, employee_list):
        self.employee = employee_list

    # 以__开头的为魔法函数，通常使用python自定义的一些魔法函数
    def __getitem__(self, item):
        return self.employee[item]

company = Company(["tom", "bob", "jane"])

employee = company.employee
for em in employee:
    print(em)

len(company)

# 添加了魔法函数__getitem__，可以直接for循环company，python会自动去查找__getitem__方法的实现，
# 依次执行，直到__getitem__方法抛出异常结束
for em in company:
    print(em)
