# 新式类
class D:
    pass

class C(D):
    pass

class B(D):
    pass

class A(B, C):
    pass

# __mro__属性记录了类的继承查找顺序
# (<class '__main__.A'>, <class '__main__.B'>, <class '__main__.C'>, <class '__main__.D'>, <class 'object'>)
print(A.__mro__)