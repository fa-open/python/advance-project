# 列表生成式（列表推导式）
# 1. 提取出1-20之间的奇数
odd_list = []
for i in range(21):
    if i%2 == 0:
        odd_list.append(i)

# 上面的循环代码，可以用下面一行代码完成
odd_list = [i for i in range(21) if i%2 == 0]

# 2. 逻辑复杂的情况
def handle_item(item):
    return item * item
# 可以在列表生成式里传入自定义方法，实现更复杂的业务逻辑
odd_list = [handle_item(i) for i in range(21) if i%2 == 0]

# 列表生成式性能高于列表操作
# 但列表生成式过于复杂的时候，不建议使用，还是要优先保证代码的可读性

# <class 'list'>
print(type(odd_list))
print(odd_list)

# 生成器表达式
odd_gen = (i for i in range(21) if i%2 == 0)
# <class 'generator'>
print(type(odd_gen))
odd_list = list(odd_gen)

print(type(odd_list))
print(odd_list)

# 字典推导式
my_dict = {"bobby1": 22, "bobby2": 23, "imooc.com": 5}
# 将字典的key和value反转
reversed_dict = {value:key for key, value in my_dict.items()}
# {22: 'bobby1', 23: 'bobby2', 5: 'imooc.com'}
print(reversed_dict)

# 集合推导式
my_set = {key for key, value in my_dict.items()}
# <class 'set'>
print(type(my_set))
print(my_set)
