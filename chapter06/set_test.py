# set 集合 frozenset（不可变集合）无序，不重复
s = set('abcde')
print(s)

# 这里要和dict的初始化方式区分开
s = {'a', 'b', 'c'}
# <class 'set'>
print(type(s))

# s = frozenset("abcde")
# frozenset({'b', 'e', 'a', 'c', 'd'})
print(s)

# 向set添加数据
s.add("a")

another_set = set("cef")
re_set = s.difference(another_set)
print(re_set)
# 集合运算：|(并集) &(交集) -(差集)
print(s - another_set)
print(s & another_set)
print(s | another_set)
# set的性能很高，背后使用的是hash（与dict类似）

# 判断一个元素是否在set中
if 'c' in s:
    print("i am in set")