from collections.abc import Mapping, MutableMapping
# dict 属于mapping类型
a ={}
# a实现了MutableMapping里定义的一些魔法函数
print(isinstance(a, MutableMapping))
