a = dict()
a = {"bobby1": {"company": "imooc"},
     "bobby2": {"company": "imooc2"}}
# clear
# a.clear()
# pass

# copy，返回浅拷贝
# new_dict = a.copy()
# # 修改new_dict，a也会改变
# # new_dict的bobby1和a的bobby1，指向同一个对象
# new_dict["bobby1"]["company"] = "imooc3"
# pass

# 深拷贝
# import copy
# new_dict = copy.deepcopy(a)
# new_dict["bobby1"]["company"] = "imooc3"
# pass

# fromkeys
new_list = ["bobby1", "bobby2"]
new_dict = dict.fromkeys(new_list, {"company": "imooc"})

# get
value = new_dict.get("bobby", {})

# for
for key, value in new_dict.items():
    print(key, value)

# setdefault
default_value = new_dict.setdefault("bobby", "imooc")

pass