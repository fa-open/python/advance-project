# python和java中的变量本质不一样，python的变量实质上是一个指针int str，便利贴

a = 1
a = 'abc'
# 1. a贴在1上面
# 2. 先生成对象，然后贴便利贴

a = [1,2,3,4]
b = a
print(id(a), id(b))
print (a is b)

a = [1,2,3,4]
b = [1,2,3,4]
print(id(a), id(b))
print (a is b)
