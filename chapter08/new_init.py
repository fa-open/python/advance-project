class User:
    # python2.2 之前没有这个方法
    # 可以自定义对象的生成，new的时候并没有生成对象
    def __new__(cls, *args, **kwargs):
        print(" in new ")
        return super().__new__(cls)

    # 在new之后调用，对象已经生成
    def __init__(self, name):
        print(" in init ")
        self.name = name

# new是用来控制对象的生成多承，在对象生成之前
# init是用来完善对象的
# 如果new方法不返回对象，则不会调用init函数
if __name__ == '__main__':
    user = User("bobby")