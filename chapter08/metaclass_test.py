# 类也是对象，type创建类的类
# 如下方法可以动态创建一个类，这个操作在java中是很难实现的。这就是动态语言的特性
def create_class(name):
    if name == "user":
        class User:
            def __str__(self):
                return "user"
        return User
    elif name == "company":
        class Company:
            def __str__(self):
                return "company"
        return Company


def say(self):
    return "i am user"
    # return self.name


class BaseClass:
    def answer(self):
        return "i am baseclass"

# 什么是元类，元类是创建类的类，对象<-class(对象)<-type(元类)


if __name__ == '__main__':
    MyClass = create_class("user")
    my_obj = MyClass()
    print(my_obj)

    # type动态创建类
    User = type("User", (), {"name": "user", "say": say})
    my_obj = User()
    print(my_obj)
    print(my_obj.name)
    print(my_obj.say())

    # type动态创建类，并且继承
    User = type("User", (BaseClass,), {"name": "user", "say": say})
    my_obj = User()
    print(my_obj.answer())
