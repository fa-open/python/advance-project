# __getattr__, __getattribute__
# __getattr__ 就是在查找不到属性的时候调用
# __getattribute__ 首先调用，一般不建议重写
from datetime import date,datetime
class User:
    def __init__(self, name, birthday):
        self.name = name
        self.birthday = birthday
        self._age = 0

    def __getattr__(self, item):
        return "not find attr"

if __name__ == '__main__':
    user = User("bobby", date(year=1987, month=1, day=1))
    print(user.name)
    print(user.age)
