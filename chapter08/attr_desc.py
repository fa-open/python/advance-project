import numbers

# 实现__get__、__set__、__delete__任意一个魔法函数，该class则是一个属性描述符
# 可以在__get__、__set__、__delete__写自定义的业务校验逻辑，很方便
class IntField:
    def __get__(self, instance, owner):
        return self.value
    def __set__(self, instance, value):
        if not isinstance(value, numbers.Integral):
            raise ValueError("int value need")
        self.value = value
    def __delete__(self, instance):
        pass

class User:
    age = IntField()

if __name__ == '__main__':
    user = User()
    user.age = '30'
    print(user.age)
