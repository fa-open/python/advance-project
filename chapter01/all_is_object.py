def ask(name="bobby"):
    print(name)

class Person:
    def __init__(self):
        print("bobby1")

# # 可以将函数赋给变量
# my_func = ask
# my_func("bobby")
#
# # 可以将类赋给变量
# my_class = Person
# my_class()

obj_list= []
obj_list.append(ask)
obj_list.append(Person)
for item in obj_list:
    print(item())
